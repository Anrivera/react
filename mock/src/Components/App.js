import React, { useState, Suspense } from "react";
//import SearchBar from "./SearchBar";
import ProductTable from "./ProductTable";
const SearchBar = React.lazy(() => import("./SearchBar"));

const useFilterTextChange = (initialValue) => {
	const [value, setValue] = useState(initialValue);
	const handleChange = (data) => setValue(data);
	return { value, onChange: handleChange };
};

const App = (props) => {
	const filterText = useFilterTextChange("");
	const inStockOnly = useFilterTextChange(false);

	return (
		<div>
			<Suspense fallback={<div>Loading...</div>}>
				<SearchBar filterText={filterText} inStockOnly={inStockOnly} />
				<ProductTable
					filterText={filterText.value}
					inStockOnly={inStockOnly.value}
					products={props.products}
				/>
			</Suspense>
		</div>
	);
};

export default App;

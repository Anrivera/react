import React from "react";

const SearchBar = (props) => {
	const filterText = props.filterText;
	const inStockOnly = props.inStockOnly;

	return (
		<div>
			<form>
				<input
					type="text"
					placeholder="Search..."
					value={filterText.value}
					onChange={(e) => filterText.onChange(e.target.value)}
				/>
				<p>
					<input
						type="checkbox"
						checked={inStockOnly.value}
						onChange={(e) => inStockOnly.onChange(e.target.checked)}
					/>{" "}
					Only show products in stock
				</p>
			</form>
		</div>
	);
};

export default SearchBar;

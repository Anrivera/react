import React, { useState } from "react";
import ComponentInput from "./ComponentInput";

const showInfo = (event, d) => {
   event.preventDefault();
   let info = document.getElementById("info");
   info.innerHTML = `<b>Nombre: </b> ${d}`;
};

const useFormInput = (initialValue) => {
   let [value, setValue] = useState(initialValue);
   const handleChange = (data) => setValue(data);
   return { value, onChange: handleChange };
};

export default function App() {
   const nombre = useFormInput("");

   return (
      <div>
         <form onSubmit={(e) => showInfo(e, nombre.value)}>
            <div>
               <label>Nombre Completo</label>
               <ComponentInput
                  type="text"
                  name="nombre"
                  id="nombre"
                  {...nombre}
               />
            </div>
            <input type="submit" value="enviar" />
         </form>
         <p id="info"></p>
      </div>
   );
} //36
//33

import React from "react";

export default function ComponentInput(props) {
   const isValid = (event) => {
      let valid = false;

      if (event.target.type === "text")
         valid = /^[A-ZÑÁÉÍÓÚ\s]+$/i.test(event.target.value);

      showMessage(event.target, valid);
   };

   const showMessage = (data, valid) => {
      let mensaje = document.getElementById("mensaje");

      mensaje.innerHTML = !data.value || !valid ? "Campo no valido" : "";

      save(data.value, valid);
   };

   const save = (data, valid) => {
      if (!data || valid) props.onChange(data);
   };

   return (
      <div>
         <input {...props} onChange={(e) => isValid(e)} />
         <p id="mensaje"></p>
      </div>
   );
} //31
//40

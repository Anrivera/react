import React, { useRef } from "react";

const App = (props) => {
	const inputEl = useRef(null);

	const onButtonClick = (e) => {
		e.preventDefault();
		// `current` apunta al elemento de entrada de texto montado
		console.log(inputEl.current);
		inputEl.current.focus();
	};

	return (
		<div>
			<input type="text" ref={inputEl} />
			<br />
			<button onClick={onButtonClick}>Focus the input</button>
		</div>
	);
};

export default App;

import React, { useRef } from "react";

const App = (props) => {
	const fileInput = useRef(null);

	const handleSubmit = (e) => {
		e.preventDefault();
		// `current` apunta al elemento de entrada de texto montado
		alert(`Selected file - ${fileInput.current.files[0].name}`);
	};

	return (
		<div>
			<form onSubmit={(e) => handleSubmit(e)}>
				<label htmlFor="upload">
					Upload file:
					<input type="file" ref={fileInput} />
				</label>
				<br />
				<button type="submit">Submit</button>
			</form>
		</div>
	);
};

export default App;

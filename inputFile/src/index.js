import React from "react";
import { render } from "react-dom";
import App from "./Components/App";

var mountNode = document.getElementById("root");
render(<App />, mountNode);

import React, { Component } from "react";

class ComponentInput extends Component {
   constructor(props) {
      super(props);

      this._isValid = this._isValid.bind(this);
   }

   _isValid = (event) => {
      let valid = false;

      if (event.target.type === "text") {
         valid = /^[A-ZÑÁÉÍÓÚ\s]+$/i.test(event.target.value);
      }

      this._message(event.target, valid);
   };

   _message = (data, valid) => {
      let mensaje = document.getElementById("mensaje");
      mensaje.innerHTML = !data.value || !valid ? "Campo no valido" : "";
      this._save(data, valid);
   };

   _save = (data, valid) => {
      console.log(data);
      if (!data || valid) this.props.setState(data);
   };

   render() {
      return (
         <div>
            <input
               type={this.props.type}
               name={this.props.name}
               id={this.props.id}
               value={this.props.value}
               onChange={(e) => this._isValid(e)}
            />
            <p id="mensaje"></p>
         </div>
      );
   }
}

export default ComponentInput;

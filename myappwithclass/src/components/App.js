import React, { Component } from "react";
import ComponentInput from "./ComponentInput";

class App extends Component {
   constructor(props) {
      super(props);

      this.state = {
         nombre: "",
      };

      this._onSubmit = this._onSubmit.bind(this);
      this._onChangeData = this._onChangeData.bind(this);
   }

   _onSubmit = (event) => {
      event.preventDefault();
      let info = document.getElementById("info");

      info.innerHTML = "<b>Nombre: </b>" + this.state.nombre;
   };

   _onChangeData = (data) => {
      let formData = {
         name: data.name,
         value: data.value,
      };

      this.setState({ [formData.name]: formData.value });
   };

   render() {
      return (
         <div>
            <form onSubmit={(e) => this._onSubmit(e)}>
               <div>
                  <label>Nombre completo</label>
                  <ComponentInput
                     type="text"
                     name="nombre"
                     id="nombre"
                     value={this.state.nombre}
                     setState={this._onChangeData}
                  />
               </div>
               <input type="submit" value="Enviar" />
            </form>
            <p id="info"></p>
         </div>
      );
   }
}

export default App;
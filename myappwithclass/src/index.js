import React from "react";
import { render } from "react-dom";
import App from "./components/App";

var mountNode = document.getElementById("root");
render(<App name="Jane" />, mountNode);
